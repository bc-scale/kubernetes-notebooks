# kubernetes-notebooks

This repository contains notebooks for learning Kubernetes basics.
They are meant to be edited during learning, to take notes and write down exercice results.

* [Intro](00-intro/) - learning goals (scope), conventions used, required tools
* [What is Kubernetes](01-what-is-kubernetes/) - create your first Kubernetes cluster and look around it
* [Kubernetes clients](02-kubernetes-clients/) - use GUI clients, like the dashboard or Lens
* [Create a pod](03-create-a-pod/) - create a pod
* [Debug a pod](04-debug-a-pod/) - find pods that are not running correctly, identify and fix the root cause
* [Managed Kubernetes](05-manage-kubneretes/) - start using a managed Kubernetes cluster
* [Expose the app](06-exposing-apps/) - expose the app to the world
* [Update the app](07-updating-apps/) - update your app
* [Run a job](08-jobs/) - run a one-time or repeated job
* [Automate the deployment](09-automatic-deployment/) - automate deploying an app on every release
* [Manage node resources](10-node-resources/) - manage finite node resources, like CPU and memory
