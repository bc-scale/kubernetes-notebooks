Managed Kubernetes
==================

After finishing this notebook, you'll learn:
* What is EKS and how is it different from `kind`
* How to create, update and delete an EKS cluster using the web console and `eksctl`

## Overview

[Amazon EKS documentation](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)
describes how to use EKS both via the web console and the `eksctl` command.

## Excercise

Run:

```
aws eks update-kubeconfig --name infotech \
  --role-arn arn:aws:iam::514111127510:role/students-eks \
  --region eu-central-1
```

Create a namespace and set it as the default one:
```
kubectl create namespace jwas
kubectl config get-contexts
kubectl config set-context <name> --namespace jwas
```

Deploy a pod used in the previous lesson.

## Test

1. What are the advantages of using EKS?
1. Which way of managing EKS clusters is easier?

## Extra

Create an EKS cluster using the web console. Configure your local `kubectl` command
and deploy an application to the cluster.

Repeat this but use the `eksctl` command.
